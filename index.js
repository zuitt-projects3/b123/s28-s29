/*
  loading the express.js module into our app and into the express
  varibale. Express module will allow us to use express.js methods to
  create our API
*/
const express = require("express");
// Create an application with express.js and stores it as an app.
// app is our server.
const app = express();

// port is a varibale to contain the port you want to designate
const port = 4000;

// express.json() - allows us to handle the requests's body and automatically
 //parse the incoming JSON to a JS object we can access in the terminal.
app.use(express.json());

let users = [
  {
    email: "mighty12@gmail.com",
    username: "mightyMouse12",
    password: "notrelatedtomickey",
    isAdmin: false
  },
  {
    email: "minnieMouse@gmail.com",
    username: "minniexmickey",
    password: "minniesincethestart",
    isAdmin: false
  },
  {
    email: "mickeyTheMouse@gmail.com",
    username: "mickeyKing",
      password: "thefacethatrunstheplace",
    isAdmin: true
  }
];

let items = [];

let loggedUser;

// Express has methods to use as routes for responding to each HTTP method
// get(<endpoint><functionToHandle requests and responses>)
app.get('/',(req,res)=>{
  // Once the route is accessed, we can send a response with the use of res.send
  // res.send() - combines res.end and res.writeHead
  // It is used to send a response to the client and is th end of the response.
  res.send("Hellow World!")
})

app.get('/hello',(req,res)=>{
  res.send("Hello from Batch 123!")
})

app.post('/',(req,res)=>{
  console.log(req.body);


  res.send(`Hello, I'm ${req.body.name}. I am ${req.body.age}. I could be described as ${req.body.description}`)
});

// register
app.post('/users',(req,res)=>{
  // it's a good practice to console log the incoming data first
  console.log(req.body);
  // simulate creation of new user document
  let newUser = {
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    isAdmin: req.body.isAdmin
  };
  users.push(newUser);
  console.log(users);
  res.send("Registered Succesfully!")
})

// login
app.post('/users/login',(req,res)=>{
  console.log(req.body);

  let foundUser = users.find((user)=>{
    return user.username === req.body.username && user.password === req.body.password;
  });

  if(foundUser){

    let foundUserIndex = users.findIndex((user)=>{
    return user.username === foundUser.username
  });
  foundUser.index = foundUserIndex
  //Temporarily log our user in. Allows us to refer the details of a logged in user
  loggedUser = foundUser;
  console.log(loggedUser);
  res.send('Thank you for Logging in!')
}else {
  loggedUser = foundUser;
  res.send('Login Failed. Wrong Credentials')
}
})

//addItem

app.post('/items',(req,res)=>{
  console.log(loggedUser);
  console.log(req.body);

  let body = req.body;

  let newItem = {
    name: body.name,
    description: body.description,
    price: body.price,
    isActive: body.isActive
  }
  if(loggedUser.isAdmin === true){
    items.push(newItem);
    console.log(items);
    res.send('You have added a new item!');
  } else {
    res.send('Unauthorize Action Forbidden')
  }
})
app.post('/items/getitems',(req,res)=>{
  if(loggedUser.isAdmin === true){
    res.send(items)
  } else {
    res.send('Unauthorize Action Forbidden')
  }
})

// getSingleUser
  // GET requests should not have a request body. It may have headers for
  // additional information or we can add or a small amount of data somewhere
  // else: the url.
  /*
    Route params are values we can pass via URL. This is done specially to allow
    us to send small amount of data into our server. Route parameters can be
    defined in the endpoint of a route with :parameterName
  */

  app.get('/users/:index',(req,res)=>{
    console.log(req.params);
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    // console.log(typeof index);
    let user = users[index];
    res.send(user)
  })

  // UpdateUser
  app.put('/users/:index',(req,res)=>{
    console.log(req.params);
    console.log(req.params.index);
    let userIndex = parseInt(req.params.index);
    if(loggedUser !== undefined && loggedUser.index === userIndex){
      // get the proper user from the array with our index
      users[userIndex].password = req.body.password;
      console.log(users[userIndex]);
      res.send('User password has been updated');
    } else {
      res.send('Unauthorize. Login the correct User first')
    }

  })
  app.get('/items/getSingle/:index',(req,res)=>{
    console.log(req.params);
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    let item = items[index]
    res.send(item)
  })

  app.put('/items/archive/:index',(req,res)=>{
    console.log(req.params);
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    if(loggedUser.isAdmin === true){
      items[index].isActive = false
      console.log(items[index]);
      res.send("Item Archived")
    } else {
      res.send('Unauthorize Action Forbidden')
    }
  })
  app.put('/items/activate/:index',(req,res)=>{
    console.log(req.params);
    console.log(req.params.index);
    let index = parseInt(req.params.index);
    if(loggedUser.isAdmin === true){
      items[index].isActive = true
      console.log(items[index]);
      res.send("Item Activated")
    } else {
      res.send('Unauthorize Action Forbidden')
    }
  })
app.listen(port, ()=>console.log(`Server is running at port ${port}`));
